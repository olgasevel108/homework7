package sample;

import jakarta.persistence.*;
import java.util.List;
import java.util.Scanner;

public class Main {
    static EntityManagerFactory emf;
    static EntityManager em;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            emf = Persistence.createEntityManagerFactory("DishesDefault");
            em = emf.createEntityManager();

            while (true) {
                System.out.println("1. Adding new Dish");
                System.out.println("2. Menu");
                System.out.println("3. Costs from and to");
                System.out.println("4. Dishes on sale");
                System.out.println("5. Weight under 1kg");
                System.out.println("6. Exit");
                System.out.print("Enter your choice: ");
                int choice = sc.nextInt();
                sc.nextLine(); // Чтение перевода строки

                switch (choice) {
                    case 1:
                        addNewDish(sc);
                        break;
                    case 2:
                        displayMenu();
                        break;
                    case 3:
                        filterByCost(sc);
                        break;
                    case 4:
                        displayDishesOnSale();
                        break;
                    case 5:
                        displayDishesUnder1kg();
                        break;
                    case 6:
                        return;
                    default:
                        System.out.println("Invalid choice. Please enter a valid option.");
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
            emf.close();
        }
    }

    private static void addNewDish(Scanner sc) {
        System.out.print("Enter dish name: ");
        String name = sc.nextLine();
        System.out.print("Enter dish price: ");
        double price = sc.nextDouble();
        sc.nextLine();
        System.out.print("Enter dish weight: ");
        double weight = sc.nextDouble();
        sc.nextLine();
        System.out.print("Is there a discount (true/false): ");
        boolean discount = sc.nextBoolean();
        sc.nextLine();

        em.getTransaction().begin();
        MenuItem newItem = new MenuItem(name, price, weight, discount);
        em.persist(newItem);
        em.getTransaction().commit();
        System.out.println("Dish added successfully.");
    }

    private static void displayMenu() {
        List<MenuItem> menuItems = em.createQuery("from MenuItem").getResultList();
        for (MenuItem item : menuItems) {
            System.out.println("Name: " + item.getName() + ", Price: " + item.getPrice() + ", Weight: " + item.getWeight() + ", Discount: " + item.isDiscount());
        }
    }

    private static void filterByCost(Scanner sc) {
        System.out.print("Enter minimum price: ");
        double minPrice = sc.nextDouble();
        sc.nextLine();
        System.out.print("Enter maximum price: ");
        double maxPrice = sc.nextDouble();
        sc.nextLine();

        List<MenuItem> items = em.createQuery("from MenuItem where price >= :minPrice and price <= :maxPrice")
                .setParameter("minPrice", minPrice)
                .setParameter("maxPrice", maxPrice)
                .getResultList();

        for (MenuItem item : items) {
            System.out.println("Name: " + item.getName() + ", Price: " + item.getPrice());
        }
    }

    private static void displayDishesOnSale() {
        List<MenuItem> items = em.createQuery("from MenuItem where discount = true").getResultList();
        for (MenuItem item : items) {
            System.out.println("Name: " + item.getName() + ", Price: " + item.getPrice() + ", Weight: " + item.getWeight());
        }
    }

    private static void displayDishesUnder1kg() {
        List<MenuItem> items = em.createQuery("from MenuItem").getResultList();
        double totalWeight = 0;
        for (MenuItem item : items) {
            if (totalWeight + item.getWeight() <= 1000) {
                System.out.println("Name: " + item.getName() + ", Weight: " + item.getWeight());
                totalWeight += item.getWeight();
            }
        }
    }
}
